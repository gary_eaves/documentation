var http = require('http');

http.createServer(function(req, res) {
  if (req.url === '/' && req.method === 'GET') {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.end('Hello <strong>home page</strong>');
  } else if (req.url === '/news' && req.method === 'GET') {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.end("Hello <strong>news page</strong>");
  } else {
    res.writeHead(404, {'Content-Type': 'text/html'});
    res.end("Page not found here governor");
  }
}).listen(5001, '127.0.0.1');
console.log('Server running at http://127.0.0.1:5001/');
