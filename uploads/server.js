var http = require('http');
http.createServer(function (req, res) {
    res.writeHead(200, { "Content-Type": "text/plain" });
    res.end('Hello from the server');
}).listen(8080, '127.0.0.1');
console.log('Server at http://127.0.0.1:5001/');
