var util = require('util');
var EventEmitter = require('events').EventEmitter;

function Vehicle(make, model, cost, colour) {

    this._make = make;
    this._model = model;
    this._cost = cost;
    this._colour = colour;
    this._speed = 0;
    var self = this;
    var distanceTravelled = 0;
    EventEmitter.call(this);
    this.startEngine = function () {

        console.log('Engine Started');
        self.emit('startEngine');


        setInterval(function () {
        self.emit('travelling', distanceTravelled);
            ++distanceTravelled;
        }, 5000);
};



};

function Telemetrics(){
    this._timesTracked = 0;
}
Telemetrics.prototype.tracker = function(car){
    this._timesTracked ++;
    console.log("tracking " + this._timesTracked + " : " +
car._make);
}
//
var tt = new Telemetrics();


util.inherits(Vehicle, EventEmitter);
var car= new Vehicle('Volkswagen','Polo','£11500','blue');

car.on('startEngine', function () {
    console.log('car started');
});

car.on('travelling', function () {
    console.log('doing travel stuff');
    tt.tracker(this);
});

car.startEngine();




